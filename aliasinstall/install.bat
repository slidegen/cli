rem Docker port of slidegen tool

echo Creating home for script in home
echo md "%userprofile%\slidegen"
md "%userprofile%\slidegen"

echo Writing script
echo docker run -it --rm --workdir "/workspace" --volume "%%userprofile%%/texmf:/root/texmf" --volume "%%cd%%:/workspace" csabasulyok/slidegen %%* >%userprofile%/slidegen/slidegen.bat

echo Adding folder to PATH
echo setx path "%PATH%;%userprofile%/slidegen"
set PATH="%PATH%;%userprofile%\slidegen"
setx PATH "%PATH%;%userprofile%\slidegen"
