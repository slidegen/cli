#!/bin/bash

cat <<EOT >>slidegen_tmp
docker run -it --rm \\
  --workdir "/workspace" \\
  --volume "\$HOME/texmf:/root/texmf" \\
  --volume "\`pwd\`:/workspace" \\
  csabasulyok/slidegen \$@
EOT

chmod a+x ./slidegen_tmp

# move to PATH
if [ "$EUID" -ne 0 ]; then
    # not sudo
    mkdir -p ~/.local/bin
    rm -rf ~/.local/bin/slidegen
    mv ./slidegen_tmp ~/.local/bin/slidegen
else
    # sudo
    rm -rf /usr/bin/slidegen
    mv ./slidegen_tmp /usr/bin/slidegen
fi