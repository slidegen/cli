#!/usr/bin/env python
"""A pandoc filter to use minted for typesetting code in the LaTeX mode."""

from string import Template
from os.path import abspath, dirname, join
from re import sub

from pandocfilters import Header, RawBlock, RawInline, toJSONFilters


supportedLanguageKeywords = [l.strip() for l in open(join(abspath(dirname(__file__)), "mintedlanguages.txt")).readlines()]


def unpack_code(value):
    """Unpack the body and language of a pandoc code element."""
    [[_, classes, attributes], contents] = value

    language = classes[0] if len(classes) > 0  and classes[0] in supportedLanguageKeywords else 'text'
    attributes = ', '.join('='.join(x) for x in attributes)

    return {'contents': contents, 'language': language, 'attributes': attributes}


def process_code_contents(contents):
    """Process inline MD code contexts to be LaTeX compatible"""
    ret = contents.replace('\\', '\\textbackslash')
    ret = ret.replace('~', '\\textasciitilde')
    ret = ret.replace('^', '\\textasciicircum')
    ret = sub(r'([_#%&${}])', r'\\\1', ret) # replace reserved characters with their protected selves
    ret = ret.replace('\\textbackslash', '\\textbackslash{}')
    ret = ret.replace('\\textasciitilde', '\\textasciitilde{}')
    ret = ret.replace('\\textasciicircum', '\\textasciicircum{}')
    return ret


def fragile(key, value, format, meta):
    """Make headers/frames fragile."""
    if format != 'beamer':
        return

    if key == 'Header':
        level, meta, contents = value
        # Add the attribute
        meta[1].append('fragile')
        return Header(level, meta, contents)


def minted(key, value, format, meta):
    """Use minted for code in LaTeX."""
    if format != 'latex' and format != 'beamer':
        return

    if key in ['CodeBlock', 'Code']:
        unpacked = unpack_code(value)

        if key == 'CodeBlock':
            template = Template('\\begin{minted}[fontsize=\\scriptsize]{$language}\n$contents\n\end{minted}')
            text = template.substitute(unpacked)
            return RawBlock('latex', text)
        else: # 'Code'
            template = Template('\\texttt{$contents}')
            unpacked['contents'] = process_code_contents(unpacked['contents'])
            text = template.substitute(unpacked)
            return RawInline('latex', text)


if __name__ == '__main__':
    toJSONFilters([fragile, minted])