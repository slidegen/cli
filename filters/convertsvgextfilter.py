#!/usr/bin/env python
'''
A pandoc filter to change extensions from SVG to PDF in processed LaTeX code.
'''

from string import Template
from pandocfilters import Image, toJSONFilters
from subprocess import call
from os import listdir, getcwd, remove, rename, walk, makedirs
from os.path import isfile, isdir, getmtime, abspath, dirname, basename, exists,\
    join


def svg2pdfName(svg):
    '''
    Default PDF file name from SVG file name.
    '''
    return "%s.pdf" %(svg[:-4])


def convertsvgext(key, value, format, meta):
    '''
    Replace any SVG extension in image inclusions to PDF
    '''
    if key == 'Image':
        [ident, [], keyvals], caption, [filename, typef] = value
        if filename.lower().endswith('.svg'):
            filename = svg2pdfName(filename)
        return Image([ident, [], keyvals], caption, [filename, typef])



if __name__ == '__main__':
    toJSONFilters([convertsvgext])