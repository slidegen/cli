#
# docker build -t csabasulyok/slidegen:staging .
#

FROM csabasulyok/texlive:latest

# install necessary tools
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -yq pandoc python3 python3-pip xzdec python3-pygments unzip inkscape

# create working directory
RUN mkdir /slidegen
WORKDIR /slidegen
ENV SLIDEGEN_HOME=/slidegen

# install pip requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

# install project
COPY . .
RUN ./install.sh

# remove unneccessary libraries
RUN apt -yq autoremove && rm -rf /var/lib/apt/lists/*

# make python command alias to python3
RUN ln -s -T /usr/bin/python3 /usr/bin/python

# set /workspace as main folder, use this as volume for sharing
RUN mkdir /workspace
WORKDIR /workspace

# set slidegen as CMD
ENTRYPOINT ["/usr/bin/slidegen"]
CMD []
