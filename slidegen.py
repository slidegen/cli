#!/usr/bin/env python

from subprocess import call
from argparse import ArgumentParser
from os import listdir, getcwd, remove, rename, walk, makedirs, getenv
from os.path import isfile, isdir, getmtime, abspath, dirname, basename, exists,\
    join, commonprefix, relpath
from sys import stderr
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from time import sleep
from tempfile import mkdtemp
from shutil import rmtree, copyfile
from re import findall, match, search, MULTILINE, IGNORECASE
from time import ctime
from termcolor import cprint, colored

try:
    from collections.abc import Iterable  # noqa
except ImportError:
    from collections import Iterable  # noqa


# detect necessary files
baseDir = getenv('SLIDEGEN_HOME', abspath(dirname(__file__)))
filterDir = join(baseDir, "filters")
defaultTemplateFileName = join(baseDir, "template.tex")

# file name for ignores
ignoreFileName = '.slidegenignore'

# file name regexps
mdFileRE = r'.+\.md'
texFileRE = r'.+\.tex'
svgFileRE = r'.+\.svg'
mdAuxFileRE = r'!\[([^\]]*)\]\(([^\)]+)\)'
texAuxFileRE = r'\\includegraphics(\[[^\]]+\])?\{([^\}]+)\}'

# file name validations
def isMdFile(fileName): return match(mdFileRE, fileName.lower()) is not None
def isTexFile(fileName): return match(texFileRE, fileName.lower()) is not None
def isSvgFile(fileName): return match(svgFileRE, fileName.lower()) is not None


def findFilterFileNames():
    '''
    Detect filter python files in filters subfolder.
    Used in pandoc call.
    '''
    ret = []
    for fileName in listdir(filterDir):
        if fileName.endswith("filter.py"):
            fileName = abspath(join(filterDir, fileName))
            ret.append(fileName)
    return ret


def flatten(items):
    '''
    Yield items from any nested iterable; see Reference.
    '''
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            for sub_x in flatten(x):
                yield sub_x
        else:
            yield x


def doesFileMatchIgnoreLine(inputName, ignoreLine, ignoreFileDir):
    '''
    Checks if filename matches line in ignore file.
    
    d('/a/b/c/d/e', 'c/', '/a')
    '''
    if match(basename(inputName), ignoreLine, IGNORECASE) != None:
        return True
    for dirName in relpath(inputName, ignoreFileDir).split('/'):
        if match(dirName + '/', ignoreLine, IGNORECASE) != None:
            return True
    return False


def isIgnored(inputName):
    '''
    Check if filename shows up in .slidegenignore file.
    '''
    ignoreDir = dirname(inputName)

    while ignoreDir != '/':
        ignoreFilePath = join(ignoreDir, ignoreFileName)
        if exists(ignoreFilePath):
            ignoreFileContent = [line.lower() for line in open(ignoreFilePath).read().splitlines()]
            for ignoreLine in ignoreFileContent:
                if doesFileMatchIgnoreLine(inputName, ignoreLine, ignoreDir):
                    return True
        ignoreDir = dirname(ignoreDir)
    
    return False


def md2texName(md):
    '''
    Default TeX file name from Markdown file name.
    '''
    return "%s.tex" %(md[:-3])

def md2pdfName(md):
    '''
    Default PDF file name from Markdown file name.
    '''
    return "%s.pdf" %(md[:-3])

def tex2pdfName(md):
    '''
    Default PDF file name from TeX file name.
    '''
    return "%s.pdf" %(md[:-4])

def svg2pdfName(svg):
    '''
    Default PDF file name from SVG file name.
    '''
    return "%s.pdf" %(svg[:-4])
    
    
def checkMTime(inputFile, outputFile):
    '''
    Check modify time of an input and output file before conversion.
    Can help omit unneccessary conversions if input file has not been changed.
    
    Returns True if conversion necessary (output doesn't exist or is older than input)
    '''
    inputMTime = getmtime(inputFile) if isfile(inputFile) else None
    outputMTime = getmtime(outputFile) if isfile(outputFile) else None
    return not isfile(inputFile) or not isfile(outputFile) or (inputMTime > outputMTime), inputMTime, outputMTime


def pandocOptions(inputFile):
    '''
    Find pandoc options (template file, output format) in Markdown file.
    Default values are specified for everything.
    '''
    opts = {
        'template': defaultTemplateFileName,
        'format': 'beamer'
    }

    inputFileContent = open(inputFile).read()
    for key in opts.keys():
        match = search(r'^%s: (.*)' %(key), inputFileContent, MULTILINE)
        if match:
            if key == 'template':
                opts[key] = abspath(join(dirname(inputFile), match.group(1))).strip()
            else:
                opts[key] = match.group(1).strip()
    
    return opts


def svg2pdf(inputFile, outputFile = None, forceRegen = False):
    '''
    Convert SVG file to PDF.
    inkscape --export-pdf=<outputFile> <inputFile>
    '''
    if outputFile is None:
        outputFile = svg2pdfName(inputFile)
        
    if not forceRegen and not checkMTime(inputFile, outputFile)[0]:
        return
                
    cmdargs = ['inkscape',
               '--export-pdf=%s' %(outputFile),
               inputFile]
    call(cmdargs)


def md2tex(inputFile, outputFile = None, forceRegen = False):
    '''
    Call pandoc to create tex file from md.
    Essentially following call:
    
    pandoc --filter=filters.py
           --template=<template>
           --to=<format>
           --output=<outputFile>
           <inputFile>
    '''
    if outputFile is None:
        outputFile = md2texName(inputFile)
        
    if not forceRegen and not checkMTime(inputFile, outputFile)[0]:
        return
    
    opts = pandocOptions(inputFile)
    cmdargs = list(flatten(['pandoc',
                           ['--filter=%s' %(filterFileName) for filterFileName in findFilterFileNames()],
                           '--template=%s' %(opts['template']),
                           '--to=%s' %(opts['format']),
                           '--output=%s' %(outputFile),
                           inputFile]))
    cprint('Calling "%s"' %(' '.join(cmdargs)), 'green')
    call(cmdargs)


def isTexOutputOutdated(inputFile, outputFile = None):
    '''
    Check if md2pdf conversion necessary
    '''
    if outputFile is None:
        outputFile = tex2pdfName(inputFile)

    return checkMTime(inputFile, outputFile)


def tex2pdf(inputFile, outputFile = None, auxFiles = None, forceRegen = False, double = False, ignoreErrors = False):
    '''
    Convert tex file to pdf using xelatex.
    Uses temp folder so original folder placement does not fill up with junk.
    Essentially following call:
    
    xelatex -shell-escape <inputFile>
    '''
    if outputFile is None:
        outputFile = tex2pdfName(inputFile)
    
    if not forceRegen and not checkMTime(inputFile, outputFile)[0]:
        return

    if auxFiles is None:
        auxFiles = findAuxFiles(inputFile, forceRegen=forceRegen)

    # create temporary workign dir
    tempDir = mkdtemp(prefix='slidegen.tmp-')
    
    # copy necessary files
    tempInputFile = join(tempDir, basename(inputFile))
    copyfile(inputFile, tempInputFile)
    if auxFiles is not None and len(auxFiles) > 0:
        for auxFile in auxFiles:
            auxFileSrc = join(dirname(inputFile), auxFile)
            if exists(auxFileSrc):
                auxFileDest = join(tempDir, auxFile)
                print("Copying aux file %s => %s" %(auxFileSrc, auxFileDest))
                if not exists(dirname(auxFileDest)):
                    makedirs(dirname(auxFileDest))
                copyfile(auxFileSrc, auxFileDest)
            else:
                print("Aux file %s does not exist, behavior unpredictable" %(auxFileSrc))
    
    # tex => pdf call
    cmdargs = ["xelatex",
               "-shell-escape",
               "-interaction=%s" %("nonstopmode" if ignoreErrors else "errorstopmode"),
               tempInputFile]
    cprint('Calling "%s"' %(' '.join(cmdargs)), 'green')
    call(cmdargs, cwd=tempDir)
    if double:
        call(cmdargs, cwd=tempDir)
    
    # copy result back
    tempOutputFile = join(tempDir, basename(outputFile))
    copyfile(tempOutputFile, outputFile)
    
    # delete temporary folder
    rmtree(tempDir)


def isMdOutputOutdated(inputFile, texFile = None, outputFile = None):
    '''
    Check if md2pdf conversion necessary
    '''
    if texFile is None:
        texFile = md2texName(inputFile)
    if outputFile is None:
        outputFile = md2pdfName(inputFile)

    texOutdated, inputMTime, _ = checkMTime(inputFile, texFile)
    pdfOutdated, _, outputMTime = checkMTime(texFile, outputFile)
    return texOutdated or pdfOutdated, inputMTime, outputMTime


def md2pdf(inputFile, texFile = None, outputFile = None, forceRegen = False, double = False, ignoreErrors = False):
    '''
    Convert Markdown file to PDF.
    Combine md2tex and tex2pdf with file naming generation and searching for auxiliary files.
    '''
    auxFiles = findAuxFiles(inputFile, forceRegen=forceRegen)
    
    if texFile is None:
        texFile = md2texName(inputFile)
    if outputFile is None:
        outputFile = md2pdfName(inputFile)
        
    md2tex(inputFile, texFile, forceRegen=forceRegen)
    tex2pdf(texFile, outputFile, auxFiles=auxFiles, forceRegen=forceRegen, double=double, ignoreErrors=ignoreErrors)
    

def findAuxFiles(inputFile, forceRegen = False):
    '''
    Finds any auxiliary files a Markdown file may be using.
    This includes external images.
    '''

    inputFileDir = dirname(inputFile)
    input = open(inputFile).read()

    if isMdFile(inputFile):
        auxFileRE = mdAuxFileRE
    elif isTexFile(inputFile):
        auxFileRE = texAuxFileRE
    else:
        return []

    auxFiles = []
    for auxFileTuple in findall(auxFileRE, input):
        auxFile = auxFileTuple[-1]
        if isSvgFile(auxFile):
            fullAuxFile = join(inputFileDir, auxFile)
            svg2pdf(fullAuxFile, forceRegen=forceRegen)
            auxFiles.append("%s.pdf" %(auxFile[:-4]))
        else:
            auxFiles.append(auxFile)
    return auxFiles
    
            

class Md2PdfEventHandler(FileSystemEventHandler):
    '''
    Watchdog event handler.
    Manages a number of MD files to convert to PDF.
    Watches given file/folder for changes.
    '''
    
    def __init__(self, inputFileNames, noRecursive = False,
                 fromTex = False, double = False, forceRegen = False,
                 ignoreErrors = False):
        inputFileNames = [abspath(i) for i in inputFileNames]
        self.noRecursive = noRecursive
        self.fromTex = fromTex
        self.double = double
        self.forceRegen = forceRegen
        self.ignoreErrors = ignoreErrors

        self.isInput = isMdFile if not self.fromTex else isTexFile
        self.isOutputOutdated = isMdOutputOutdated if not self.fromTex else isTexOutputOutdated
        self.input2Pdf = md2pdf if not self.fromTex else tex2pdf
        
        self.inputFiles = []

        for inputFileName in inputFileNames:
            if isfile(inputFileName):
                if self.isInput(inputFileName):
                    self.inputFiles.append(inputFileName)
                else:
                    cprint("Error! %s not in appropriate format" %(inputFileName), 'red', file=stderr)
                    
            elif isdir(inputFileName):
                print("Using directory %s" %(inputFileName))
                inputDir = inputFileName
                
                if self.noRecursive:
                    for inputFileName in listdir(inputDir):
                        inputFileName = abspath(join(inputDir, inputFileName))
                        if self.isInput(inputFileName) and not isIgnored(inputFileName):
                            self.inputFiles.append(inputFileName)
                else:
                    for root, _, inputFileNames in walk(inputDir):
                        for inputFileName in inputFileNames:
                            inputFileName = abspath(join(root, inputFileName))
                            if self.isInput(inputFileName) and not isIgnored(inputFileName):
                                self.inputFiles.append(inputFileName)

                if len(self.inputFiles) == 0:
                    print('No appropriate input files found')
            
            else:
                cprint("Error! %s not found" %(inputFileName), 'red', file=stderr)
            
    
    def on_moved(self, event):
        #print "on_moved", event
        super(Md2PdfEventHandler, self).on_moved(event)
        oldFilePath = event.src_path
        newFilePath = event.dest_path
        if oldFilePath in self.inputFiles:
            print('%s moved to %s, moving its output file' %(oldFilePath, newFilePath))
            self.inputFiles.remove(oldFilePath)
            self.inputFiles.append(newFilePath)
            if exists(md2pdfName(oldFilePath)):
                rename(md2pdfName(oldFilePath), md2pdfName(newFilePath))
            if exists(md2texName(oldFilePath)):
                rename(md2texName(oldFilePath), md2texName(newFilePath))

    def on_created(self, event):
        #print "on_created", event
        super(Md2PdfEventHandler, self).on_created(event)
        filePath = event.src_path
        if self.isInput(filePath) and not isIgnored(filePath):
            self.inputFiles.append(filePath)
            print('New file %s, creating its output file' %(filePath))
            self.input2Pdf(filePath, double=self.double, ignoreErrors=self.ignoreErrors)

    def on_deleted(self, event):
        #print "on_deleted", event
        super(Md2PdfEventHandler, self).on_deleted(event)
        filePath = event.src_path
        if filePath in self.inputFiles:
            print('%s deleted, also deleting output' %(filePath))
            if isMdFile(filePath):
                if exists(md2texName(filePath)):
                    remove(md2texName(filePath))
                if exists(md2pdfName(filePath)):
                    remove(md2pdfName(filePath))
            else:
                if exists(tex2pdfName(filePath)):
                    remove(tex2pdfName(filePath))
            
    def on_modified(self, event):
        #print "on_modified", event
        super(Md2PdfEventHandler, self).on_modified(event)
        filePath = event.src_path
        if filePath in self.inputFiles:
            print('%s modified, recreating its output' %(filePath))
            self.input2Pdf(filePath, double=self.double, ignoreErrors=self.ignoreErrors)


    def printInputInfo(self):
        '''
        print(information on files)
        '''
        cprint('Status:', attrs=['bold'])
        for mdFile in self.inputFiles:
            outdated, inputMTime, outputMTime = self.isOutputOutdated(mdFile)
            status = colored('(needs update)', 'red') if outdated else colored('(up-to-date)', 'green')
            cprint('  %s %s' %(relpath(mdFile, getcwd()), status), attrs=['bold'])
            inputMTime = ctime(inputMTime) if inputMTime is not None else None
            outputMTime = ctime(outputMTime) if outputMTime is not None else None
            print('    inputMTime: %s, outputMTime: %s' %(inputMTime, outputMTime))


    def performAll(self):
        '''
        Perform conversion for all files.
        '''
        for mdFile in self.inputFiles:
            self.input2Pdf(mdFile, forceRegen=self.forceRegen, double=self.double, ignoreErrors=self.ignoreErrors)
    
    
    def watchForChanges(self):
        '''
        Start observer watching for changes in configured files.
        '''
        cp = commonprefix(self.inputFiles)
        if not exists(cp) or isfile(cp):
            cp = dirname(cp)

        print('Watching %s for changes' %(cp))
        observer = Observer()
        observer.schedule(self, cp, recursive=(not self.noRecursive))
        observer.start()
        try:
            while True:
                sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()
        


if __name__ == '__main__':

    # parse command line arguments
    parser = ArgumentParser(description='Create presentation slides from Markdown')

    parser.add_argument('input', nargs='*', default=[ getcwd() ],
                        help='Markdown file or folder to convert to slides (default: current dir)')
    parser.add_argument('-t', '--from-tex', action='store_true',
                        help='Scan for TeX files instead of Markdown')
    parser.add_argument('-d', '--double', action='store_true',
                        help='Call pdflatex/xelatex twice for caching/correct page numbers')
    parser.add_argument('-n', '--no-recursive', action='store_true',
                        help='If set and input is folder, only searches current folder non-recursively')
    parser.add_argument('-f', '--force-regen', action='store_true',
                        help='Force PDF regeneration even if output older than input')
    parser.add_argument('-i', '--ignore-errors', action='store_true',
                        help='Ignore errors in TeX compilation')
    parser.add_argument('-w', '--watch', action='store_true',
                        help='If set, program does not exit but watches files for changes')
    parser.add_argument('-l', '--list', action='store_true',
                        help='Only list files which would be used for generation')

    args = parser.parse_args()

    # parse input files/folders
    handler = Md2PdfEventHandler(inputFileNames = args.input,
                                 noRecursive = args.no_recursive,
                                 fromTex = args.from_tex,
                                 double = args.double,
                                 forceRegen = args.force_regen,
                                 ignoreErrors = args.ignore_errors)
    
    if args.list:
        handler.printInputInfo()
    else:
        handler.performAll()
    
    # watch if necessary
    if args.watch:
        handler.watchForChanges()
        
        