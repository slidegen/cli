#!/bin/bash

# --------------------------------------------------------
# install script which runs "slidegen" to path
# --------------------------------------------------------

# create runner script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
chmod a+x $DIR/slidegen.py
echo "export SLIDEGEN_HOME=${DIR}" >>~/.bashrc

# move to PATH
if [ "$EUID" -ne 0 ]; then
    # not sudo
    mkdir -p ~/.local/bin
    rm -rf ~/.local/bin/slidegen
    ln -s $DIR/slidegen.py ~/.local/bin/slidegen
else
    # sudo
    rm -rf /usr/bin/slidegen
    ln -s $DIR/slidegen.py /usr/bin/slidegen
fi