---
theme: Dresden

title: My title
subtitle: My subtitle
author: My author
date: November 7, 2018
...

# Slide 1

- bullet point 1
- bullet point 2
- bullet point 3
- bullet point 4

# Slide 2

- here we have some *italic text*
- here we have some **bold text**
- here we have some `monospace text`

```python
print("Hello, World!")
```