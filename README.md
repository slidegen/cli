# Markdown slide generator

- uses pandoc to convert Markdown to Beamer LaTeX files
- uses TeXLive to compile PDFs

# Necessary tools

- python
- pandoc
- TeXLive with XeTeX
- alternatively Docker

# Installation

```bash
pip install -r requirements.txt
./install.sh
```

# Usage

```
usage: slidegen [-h] [--watch] [inputFile]

Create Codespring presentation slides from Markdown

positional arguments:
  inputFile    Markdown file or folder to convert to slides (default: current
               dir)

optional arguments:
  -h, --help   show this help message and exit
  --watch, -w  If set, program does not exit but watches files for changes
```
See *examples* subfolder for more.

# Running through Docker

1. Create shareable `texmf` folder in **your user's home directory**. Place here any dependent themes/libraries you may need.
2. Alias the `slidegen` command using a script in `aliasinstall`.
3. Restart console, try out `slidegen` command.
